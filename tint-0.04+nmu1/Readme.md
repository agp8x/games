# Building with Docker

## Docker

```bash
docker build -t tint-build .
docker run -v ${PWD}/:/target/ tint-build
```

## Docker Compose

`docker-compose run --rm tint-build`
